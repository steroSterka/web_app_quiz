import 'package:flutter/material.dart';

class MyWidget extends StatelessWidget {
  const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MyWidget'),
      ),
      body: Center(
        child: OutlinedButton(onPressed: ()=> Navigator.of(context).pop(), child: Text('Go back!')),
      ),
    );
  }
}